# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/gazebo-test-coverage

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-archived-gh-pages/#!/osrf/gazebo-test-coverage

## Until May 31st 2020, the mercurial repository can be found at

https://bitbucket.org/osrf-migrated/gazebo-test-coverage
